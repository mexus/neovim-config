" Don't save backup/undo files
set nobackup
set noundofile

" Show line numbers.
set number

" Tabulation options
set expandtab
set shiftwidth=4
set softtabstop=4

" Hidden buffers mode on
set hidden

" Save status of a file
set laststatus=2

" Set maximum simultaneously opened tabs
set tabpagemax=100

" Incremental search
set incsearch

" Activate clipboard
set clipboard+=unnamedplus

" Map buffer switching
map <C-L> :bn<CR>
map <C-H> :bp<CR>

" Saving a cursor position
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" Show relative line numbers and switch it with \rl
set relativenumber
map <leader>rl :set relativenumber!<cr>

" Treat `.h` files as `C` instead of default `C++`
autocmd BufNewFile,BufRead *.h set filetype=c
