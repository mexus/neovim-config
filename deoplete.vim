" Deoplete completion
let g:deoplete#enable_at_startup = 1
call deoplete#custom#option('smart_case', v:true)
call deoplete#custom#option('camel_case', v:true)

call deoplete#custom#option('sources', {
\ '_': ['buffer'],
\ 'cpp': ['buffer', 'LanguageClient'],
\ 'c': ['buffer', 'LanguageClient'],
\ 'rust': ['buffer', 'LanguageClient'],
\ 'python': ['buffer', 'LanguageClient'],
\ 'python3': ['buffer', 'LanguageClient'],
\ 'vim': ['vim'],
\})

inoremap <silent><expr> <C-C>
    \ pumvisible() ? "\<C-n>" :
    \ <SID>check_back_space() ? "\<TAB>" :
    \ deoplete#mappings#manual_complete()

function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~ '\s'
endfunction

autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif

call deoplete#custom#source('_',
            \ 'disabled_syntaxes', ['Comment', 'String'])

function g:Multiple_cursors_before()
  call deoplete#custom#buffer_option('auto_complete', v:false)
endfunction

function g:Multiple_cursors_after()
  call deoplete#custom#buffer_option('auto_complete', v:true)
endfunction
