# Neovim setup

## Fresh setup

1. Install colors package for neovim (or vim). Reffer to `visuals.vim` to see which color scheme is
   used in the current configuration.
1. Make sure python 2&3 modules for neovim are installed! For instance, if you've got `python2` and
  `python3` in your `PATH` for python v2 and python v3 respectively, run
   ```sh
   $ python3 -c "import neovim" && python2 -c "import neovim" && echo "Ok!"
   ```
   .. to see whether the modules are available or not.
1. Set up python providers (see below).
1. Clone this repo into `~/.config/nvim/`.
1. Install `vim-plug` (see below).
1. Install the plugins: `nvim +PlugInstall +UpdateRemotePlugins +qa`.


## Python providers

To be on the safe side it is recommended to provide full paths to python executables.
Since the paths are different on different hosts it makes sense to put that data into
`local.vim` file which is ignore in `.gitignore`.

Here's an example of how it looks on my linux machine:

```viml
" Python providers
let g:python_host_prog = '/usr/bin/python2'
let g:python3_host_prog = '/usr/bin/python3'
```

## `vim-plug` installation

.. is as simple as the following:

```
% curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```
