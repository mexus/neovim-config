" Tagbar is a Vim plugin that provides an easy way to browse the tags of the
" current file and get an overview of its structure.

nnoremap <Leader>tt :TagbarToggle<CR>
nnoremap <Leader>ff :Files<CR>
