call plug#begin()
    " The famous NERDTree
    Plug 'scrooloose/nerdtree'

    " Git tools:
    Plug 'tpope/vim-fugitive'
    Plug 'Xuyuanp/nerdtree-git-plugin'
    Plug 'airblade/vim-gitgutter'

    " `diff` for directories inside the vim
    Plug 'will133/vim-dirdiff'

    " Hightlight whitespaces at the end of a line
    Plug 'ntpeters/vim-better-whitespace'

    " Show bookmarks
    Plug 'kshenoy/vim-signature'

    " Switch colors easily
    Plug 'xolox/vim-colorscheme-switcher'

    " Dependency of 'xolox/vim-colorscheme-switcher'
    Plug 'xolox/vim-misc'

    " Multiple cursors
    Plug 'terryma/vim-multiple-cursors'

    " Airline plugin (like powerline)
    Plug 'vim-airline/vim-airline'

    " Command-line fuzzy finder
    Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
    Plug 'junegunn/fzf.vim'

    " Search selection
    Plug 'nelstrom/vim-visual-star-search'

    " Rust basic support
    Plug 'rust-lang/rust.vim'

    " Toml files highlighting
    Plug 'cespare/vim-toml'

    " Language client server
    Plug 'autozimu/LanguageClient-neovim', {
        \ 'branch': 'next',
        \ 'do': 'bash install.sh',
        \ }

    " Deoplete code completion
    Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }

    " *vim* source for deoplete
    Plug 'Shougo/neco-vim'

    " `clang-format` support
    Plug 'rhysd/vim-clang-format'

    " " Snippets
    " Plug 'Shougo/neosnippet.vim'

    " " Default snippets
    " Plug 'Shougo/neosnippet-snippets'

    " Terminus enhances Vim's and Neovim's integration with the terminal ...
    Plug 'wincent/terminus'

    " Displays function signatures from completions in the command line.
    Plug 'Shougo/echodoc.vim'

    " Small colorschemes collection
    Plug 'rafi/awesome-vim-colorschemes'

    " Tagsbar
    Plug 'majutsushi/tagbar'

    " Syntastic
    Plug 'vim-syntastic/syntastic'
call plug#end()

source $NVIM_CONFIG_PATH/nerdtree.vim
source $NVIM_CONFIG_PATH/vim-fugitive.vim
source $NVIM_CONFIG_PATH/vim-airline.vim
source $NVIM_CONFIG_PATH/fzf.vim
source $NVIM_CONFIG_PATH/rust.vim
source $NVIM_CONFIG_PATH/language-client.vim
source $NVIM_CONFIG_PATH/deoplete.vim
source $NVIM_CONFIG_PATH/vim-clang-format.vim
source $NVIM_CONFIG_PATH/tagsbar.vim
source $NVIM_CONFIG_PATH/syntastic.vim

" set cmdheight=3
set noshowmode
let g:echodoc#enable_at_startup = 1
let g:echodoc#type = "floating"
