" Color scheme
colorscheme space-vim-dark

" Mouse interraction
set mouse+=a

" Set update interval to be more responsive
set updatetime=100
