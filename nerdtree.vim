" Plug 'scrooloose/nerdtree'
let NERDTreeShowLineNumbers=1

map <Leader>o :NERDTreeFind<CR>

" NERDTress File highlighting
function! NERDTreeHighlightFile(extension, fg, bg)
    exec 'autocmd filetype nerdtree highlight ' . a:extension .' ctermbg='. a:bg .' ctermfg='. a:fg
    exec 'autocmd filetype nerdtree syn match ' . a:extension .' #^\s\+.*'. a:extension .'$#'
endfunction

call NERDTreeHighlightFile('cpp', '10', 'none')
call NERDTreeHighlightFile('h', '43', 'none')
