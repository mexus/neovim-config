" Language client (autozimu/LanguageClient-neovim)

let g:LanguageClient_autoStart = 0

autocmd FileType rust,cpp,c call SetupLanguageClient()
autocmd FileType c,cpp call SetupClangD()

function SetupClangD()
    let g:LanguageClient_serverStderr = '/tmp/clangd.stderr'
endfunction

function SetupLanguageClient()
        "\ 'rust': ['ra_lsp_server'],
        " \ 'rust': ['rustup', 'run', 'nightly', 'rls'],
    let g:LanguageClient_serverCommands = {
        \ 'rust': ['ra_lsp_server'],
        \ 'c': ['clangd', '-background-index'],
        \ 'cpp': ['clangd', '-background-index'],
        \ 'haskell': ['hie', '--lsp', '--logfile=/tmp/hie.log'],
        \ }

    " let g:LanguageClient_loggingFile = '/tmp/LanguageClient.log'
    " let g:LanguageClient_serverStderr = '/tmp/lsp-stderr.log'

    let g:LanguageClient_autoStart = 1

    let g:LanguageClient_loadSettings = 1
    let g:LanguageClient_settingsPath = '.language-client.json'

    let g:LanguageClient_rootMarkers = {
        \ 'rust': ['Cargo.lock'],
        \ 'c': ['.language-client.json'],
        \ 'cpp': ['.language-client.json'],
        \ }

    noremap <silent> K :call LanguageClient#textDocument_hover()<CR>
    noremap <silent> <C-G> :call LanguageClient#textDocument_definition()<CR>
    noremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>

    nnoremap <leader>rr :call LanguageClient#textDocument_references()<CR>
    vnoremap <buffer> <leader>cf :call LanguageClient#textDocument_rangeFormatting()<CR>
    nnoremap <buffer> <leader>cf :call LanguageClient#textDocument_formatting()<CR>

    set formatexpr=LanguageClient#textDocument_rangeFormatting()
endfunction
