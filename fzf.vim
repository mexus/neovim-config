" Command-line fuzzy finder
nnoremap <Leader>ff :Files<CR>
nnoremap <Leader>gf :GFiles<CR>
nnoremap <Leader>ll :Lines<CR>
nnoremap <Leader>bf :Buffers<CR>

imap <c-x><c-f> <plug>(fzf-complete-path)
" imap <c-x><c-j> <plug>(fzf-complete-file-ag)
imap <c-x><c-k> <plug>(fzf-complete-word)
imap <c-x><c-l> <plug>(fzf-complete-line)
