" Plug 'tpope/vim-fugitive'
function! FugitiveGrepImpl(...)
    exec 'Ggrep ' . join(a:000)
    exec 'copen'
endfunction

command -nargs=* FugitiveGrep :call FugitiveGrepImpl(<f-args>)

map <Leader>gg :FugitiveGrep
