let $NVIM_CONFIG_PATH = expand('<sfile>:p:h')

" " Treat `.h` files as `C` instead of default `C++`
" autocmd BufNewFile,BufRead *.h set filetype=c

source $NVIM_CONFIG_PATH/plugins.vim
source $NVIM_CONFIG_PATH/common.vim
source $NVIM_CONFIG_PATH/visuals.vim

if filereadable(expand("$NVIM_CONFIG_PATH/local.vim"))
    source $NVIM_CONFIG_PATH/local.vim
endif
